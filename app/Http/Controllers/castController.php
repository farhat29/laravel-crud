<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function movies(Request $request){
        $request->validate([
            'nama' => 'required|unique:post',
            'deskripsi' => 'required',
        ]);   
        
        DB::table('cast')->insert(
            [
            'nama' => $request['nama'], 
            'deskripsi' => $request['deskripsi ']
            ]
        );

        return redirect('/cast/create');

    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:post',
            'deksripsi' => 'required',
        ]);

        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(
            [
                  'nama' => $request['nama'],
                  'deksripsi' => $request['deksripsi']
            ]
                );


        return redirect('/cast');
    }


    public function destroy($id){
        DB::table('user')->where('id', $id)->delete();

        return redirect('/cast');

    }

}
