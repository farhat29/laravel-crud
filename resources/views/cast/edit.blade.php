@extends('layout.master')

@section('judul')
Edit Cast {{$cast->nama}}
@endsection

@section('content')
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf 
            @method('PUT')
            <div class="form-group">
                <label>nama</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>dekskripsi</label>
                <textarea name="dekskripsi" class="form-control" cols="30" rows="10">value="{{$cast->dekskripsi}}"</textarea>
                @error('dekskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection