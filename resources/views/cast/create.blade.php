@extends('layout.master')

@section('judul')
Tambah Cast
@endsection

@section('content')
    <h2>Tambah Data</h2>
        <form action="/posts" method="POST">
            @csrf
            <div class="form-group">
                <label>nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>dekskripsi</label>
                <textarea name="dekskripsi" class="form-control" cols="30" rows="10"></textarea>
                @error('dekskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection